var input = document.querySelector('input[name=query]');
var searchBtn = document.getElementById("searchBtn");

function EnDisSearchBtn() {
  if (input.value.length > 1) {
    searchBtn.disabled = false;
  } else {
    searchBtn.disabled = true;
  }
}
