"""`main` is the top level module for your Flask application."""
# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

from flask import Flask, render_template, request
import unirest
import json

app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500


@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template("index.html")


@app.route('/result', methods=['GET', 'POST'])
def result():
    query = request.form["query"]
    ratio = request.form["ratio"]

    # print '|||||||||||||||||'
    # print bool(ratio), ratio
    # print '|||||||||||||||||'

    # return render_template("result.html")

    if ratio:
        ratio = float(ratio)

    try:
        response = unirest.post(
            "https://wt-b21f44bef0dd96051fbbf3ae155fa84c-0.run.webtask.io/twitter-mates",
            headers={"Accept": "application/json",
                     "Content-Type": "application/json"},
            params=json.dumps({
                "query": query,
                "ratio": ratio
            })
        )

        resp = response.body["result"]

        return render_template("result.html", resp=resp)

    except:
        return render_template("index.html", error="Couldn't find anyone")

# if __name__ == "__main__":
#     app.debug = False
#     app.run()
